#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e
set -x

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

. "${TESTDIR}/common/update-test-path"

cleanup() {
    systemctl stop geoclue.service

    if [ -n "$SERVICE_CONF_FILE" ]; then
        rm $SERVICE_CONF_FILE
        # Reload now that the temp config file has been removed
        systemctl daemon-reload
    fi

    systemctl start geoclue.service

    if [ -n "$AGENT_PID" ]; then
        kill $AGENT_PID
    fi
}

if [ $# -ne 1 ] || { [ "$1" != normal ] && [ "$1" != malicious ] ; }
then
    echo "Usage: $0 <normal|malicious>"
    exit 1
fi

# This expects to run as root.
if [ "$(id -u)" -ne 0 ]; then
    echo "$0 must run as root."
    exit 2
fi

trap cleanup EXIT

/usr/libexec/geoclue-2.0/demos/agent &
AGENT_PID=$!

systemctl stop geoclue.service

if [ "$1" = "malicious" ]; then
    # Create temporary service conf file
    SERVICE_CONF_DIR="/run/systemd/system/geoclue.service.d/"
    mkdir -p $SERVICE_CONF_DIR

    SERVICE_CONF_FILE=$SERVICE_CONF_DIR"apertis-tests-temporary.conf"
    echo "[Service]" > $SERVICE_CONF_FILE
    echo "Environment=LD_PRELOAD=/usr/lib/apertis-tests/apparmor/geoclue/libgeoclue-malicious-override.so" >> $SERVICE_CONF_FILE

    # Load this temp file
    systemctl daemon-reload
fi

systemctl start geoclue.service

gdbus wait -t 5 -y "org.freedesktop.GeoClue2"
/usr/libexec/geoclue-2.0/demos/where-am-i

# Check it is actually confined.
pid=`systemctl show --property MainPID --value geoclue`
confinement=`cat /proc/${pid}/attr/current`
if [ "$confinement" != "$(readlink -f /proc/${pid}/exe) (enforce)" ]; then
    echo "Invalid confinement: $confinement"
    exit 3
fi

sleep 5

trap - EXIT
cleanup

# Lava compatibility:
# systemctl status will exit 0 if service is running, non-zero otherwise
systemctl status geoclue.service
exit $?
