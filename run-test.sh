#!/bin/sh
# vim: tw=0

set -eu

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

. "${TESTDIR}/common/update-test-path"

# We want to use the pre-existing session bus.
export RUN_AS_USER=no
export LAUNCH_DBUS="no"

error=0
"${TESTDIR}"/common/run-aa-test "${TESTDIR}"/geoclue.malicious.expected "${TESTDIR}"/geoclue malicious || error=$?
"${TESTDIR}"/common/run-aa-test "${TESTDIR}"/geoclue.normal.expected "${TESTDIR}"/geoclue normal || error=$?

if [ $error = 0 ]; then
  echo "# apparmor-geoclue: all tests passed"
else
  echo "# apparmor-geoclue: at least one test failed"
fi

exit $error
